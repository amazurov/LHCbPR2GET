#!/usr/bin/env bash

if ps -ef | grep -v grep | grep lhcbpr2get.py ; then
        exit 0
fi

export GROUP_DIR=/afs/cern.ch/group/z5
export X509_USER_PROXY=/opt/dirac/pro/work/ResourceStatus/LHCbPRProxyAgent/.shifterCred

source $GROUP_DIR/group_login.sh 1>/dev/null
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

lb-run -c x86_64-slc6-gcc49-opt LHCbDirac/prod $CURRENT_DIR/lhcbpr2get.py --output /lhcbprdata/zips
cd /app && docker-compose exec -T lhcbpr2be ./site/manage.py lhcbpr_import /lhcbprdata/zips --period 7
