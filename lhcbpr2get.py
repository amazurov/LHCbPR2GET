#!/usr/bin/env python

import sys
import os
import stat
import argparse
import grp
import logging
import subprocess


logging.basicConfig(format='%(levelname)s:lhcbpr2get:%(message)s',
                    level=logging.DEBUG)

DEFAULT_SOURCE = '/lhcb/prdata/zips'
DEFAULT_GROUP = 'lhcbpr'
DIRAC_STORAGEELEMENT = 'StatSE'

from DIRAC.Core.Base.Script import initialize
initialize(ignoreErrors=True, enableCommandLine=False)


from DIRAC.Resources.Storage.StorageElement import StorageElement


def parse_cmd():

    parser = argparse.ArgumentParser(description='Download files from DIRAC')
    parser.add_argument('--source',
                        help='DIRAC source directory (default: "/lhcb/prdata/zips")',
                        default='/lhcb/prdata/zips')

    parser.add_argument(
        '--output', help='Output directory (default: current directory)')

    return parser.parse_args()


def download(source, output):
    gid = grp.getgrnam(DEFAULT_GROUP).gr_gid
    real_output = os.path.realpath(output)
    statSE = StorageElement(DIRAC_STORAGEELEMENT)
    lst = statSE.listDirectory(source)

    existing_files = dict(map(lambda x: (x, True), os.listdir(real_output)))
    logging.info("%d file(s) at the storage element" %
                 len(lst['Value']['Successful']))
    for item in lst['Value']['Successful'].iteritems():
        folder, folder_items = item
        for file in folder_items['Files']:
            if file in existing_files:
                continue
            logging.info("New file at the storage element %s" % file)
            real_file = os.path.join(folder, file)

            # subprocess.check_call(["dirac-dms-get-file", real_file, os.path.realpath(real_output)])
            statSE.getFile(real_file, os.path.realpath(real_output))

            # local = os.path.join(real_output, file)
            # os.chown(local, -1, gid)
            # st = os.stat(local)
            # os.chmod(local, st.st_mode | stat.S_IWGRP)


def main():
    cmd = parse_cmd()
    download(source=cmd.source, output=cmd.output or '.')
    return 0

if __name__ == '__main__':
    sys.exit(int(main() or 0))
